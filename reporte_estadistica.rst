Como generar un reporte de estadísticas
***************************************

En el menú de **Reportes** - *Estadisticas* seguimos los siguientes pasos:

.. image:: images/reporte_estadisticas/1.png
        :align: center

1. Escoger el intervalo de fecha para el reporte.

2. click en *Buscar*.

Como descargar el reporte del estadística
*****************************************

Puede descargar dos tipos de formato .xlsx (excel) o .csv

.. image:: images/reporte_estadisticas/2.png
        :align: center

3. Click en el formato de preferencia. 