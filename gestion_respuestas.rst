Como crear respuestas rápidas
=============================

En el menú de **Gestión** - *Respuestas rápidas* seguimos los siguientes pasos:

.. image:: images/crear_respuestas/1.PNG
        :align: center

1. Crear nueva respuesta.

.. image:: images/crear_respuestas/2.PNG
        :align: center

2. Aquí se colocará el atajo.

3. Nombre general del atajo. 

4. Contenido del atajo.

5. Guardar.

Como usar las  respuestas rápidas
=================================

Los agentes podrá usar una vez creadas las repuestas rápidas recargando el navegador con **F5** 
, por lo que en un chat con algun cliente debe:

.. image:: images/crear_respuestas/3.PNG
        :align: center

1. Usar el comando */* para invocar a los atajos existentes. Estos se mostrarán arriba 
de la barra de escritura del chat. 

2. Puede selecionar el atajo con el mouse dando click sobre él. Por otro lado, si no encuentra 
el atajo puede escribir el nombre de la invocacion del atajo (punto 2 en la creación del atajo) 
y presionar *Enter* . De la siguiente forma:


.. image:: images/crear_respuestas/4.PNG
        :align: center





