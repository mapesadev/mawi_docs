Como crear una calificación
===========================

En el menú de **Gestión** - *Calificaciones* seguimos los siguientes pasos:

.. image:: images/crear_calificaciones/1.PNG
        :align: center

1. Nueva calificación.

.. image:: images/crear_calificaciones/2.PNG
        :align: center

2. Nombre de calificación.

.. note:: 

        Si desea usar un sofware externo para los datos de la calificación marque 
        *público*, de lo contrario deje sin marcar.

3. Guardar. 

.. image:: images/crear_calificaciones/3.PNG
        :align: center

4. Editar calificación.

.. image:: images/crear_calificaciones/4.PNG
        :align: center

5. Agregar formulario.

6. Guardar.

.. image:: images/crear_calificaciones/5.PNG
        :align: center

7. Activar formulario.

