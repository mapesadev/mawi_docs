Como crear  un horario
======================

En el menú de **Horarios** - *Fuera de Horario* seguimos los siguientes pasos:

.. image:: images/crear_horarios/1.PNG
        :align: center

1. Crear nuevo horario.

.. image:: images/crear_horarios/2.PNG
        :align: center

2. Agregar horario de *Inicio*

3. Agregar horario de *Fin*

4. agregar xona horaria.

5. selecione los días en que se moestraran los horarios fuera de atención.

.. note::

        Para seleccionar varios dias debe mantener presionado *Control* y darle 
        click sobre el día.

1. Contenido del mensaje al cliente. 

.. note::

        Cada ve que un cliente escriba en el horario fuera de servicio se le mandará 
        el mensaje automático.

7. Guardar. 