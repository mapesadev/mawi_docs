Historial
*********

El reporte del *Historial* muestra la información de las atenciones generadas por los agentes 
en un intervalo de fechas selecionados. esto es una tabla que nos muestra contenidos como 
el agente que recibió la atención, fecha de inicio de la atención, fecha de cierre de la 
atención, tiempo promedio de conversación o duración de la atención, y hasta se puede ver 
en la culumna de *Ver* lo que ocurrió en dicha atención. 

.. image:: images/reporte_historial/historial.png
        :align: center

*Figura 2: Reporte de historial*

.. toctree::

   reporte_historial.rst

Estadísticas
************

El reporte de *Estadísticas* muestra algunas datos referido con las atenciones como 
atenciones abiertos, atenciones cerradas, atenciones iniciadas por el agente, atenciones 
iniciadas por el cliente, el número de atenciones por agentes, etc, en un determinado 
intervalo de fecha.

.. image:: images/reporte_estadisticas/estadistica.png
        :align: center

*Figura 3: Reporte de estadística*

.. toctree::

   reporte_estadistica.rst

Gestión
*******

El reporte de *Gestion*  nos permite ver la información adquirida por lo agentes de los usuarios. 
Es decir, es como vemos la información de los datos en los distintos formularios. Esto es en un 
intervalo de fecha escogido.

.. image:: images/reporte_gestion/gestion.png
        :align: center

*Figura 4: Reporte de gestión*

.. toctree::

   reporte_gestion.rst


Transferencias
**************

El reporte de *Transferencia* muestra las tranferencias de las atenciones hechas o recibidas 
entre los agentes, o ya sea que el supervisor le ha asignado una atención a uno agente en especial. 
Este reporte es en un intervalo de fecha selecionado. 

.. image:: images/reporte_transferencia/transferencia.png
        :align: center

*Figura 5: Reporte de transferencia*

.. toctree::

    reporte_transferencias.rst

Session
*******

El reporte de *sesión* muestra el tiempo total de conexión de un agente en la App 
de `Mawi <https://www.mawi.chat/>`_ en un intervalo de fechas escogidas. Donde se muestra 
cada día cuales fueron sus horas en la App. 

.. image:: images/reporte_session/session1.png
        :align: center

.. image:: images/reporte_session/session2.png
        :align: center

*Figura 6: Reporte de sesión*

.. toctree::

    reporte_session.rst


Productividad
*************

El reporte de *Productividad* del agente muestra una serie de gráficas que ayudan a visualizar 
las gestiones hechas por un agente en un determinado intervalo de tiempo; ya sean, atenciones 
abiertas, cerradas, generadas, recibids, tiempos promedios de converzación y tiempos promedios 
de primera respuesta y número de sesiones, en cada uno de los dias seleccionados en el intervalo. 

.. image:: images/reporte_productividad/productividad.png
        :align: center

*Figura 7: Reporte de productividad*

.. toctree::

    reporte_productividad.rst

Linea de tiempo
***************

La *Línea de tiempo* (o Timeline) es un reporte el cual permite ver el rastro de las diferentes 
acciones que puede tener una atención desde que se inicia. Como el momento en donde se atenció,
si se transfirió y a qué agente, o la hora de cierre, entre otros. Esto con los detalles pertinentes 
al tiempo en el que se realizarón las diferentes acciones. 

.. image:: images/reporte_timeline/timeline.png
        :align: center

*Figura 8: Reporte de historial*

.. toctree::

    reporte_linea_de_tiempo.rst

.. note::

        Recuerde que todos los reportes pueden descargarse en formato *.xlsx* o .*csv* , 
        excepto el de línea de tiempo. 