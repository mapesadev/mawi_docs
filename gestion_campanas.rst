Como crear una campañas
=======================

En el menú de **Gestión** - *Calificaciones* seguimos los siguientes pasos:

.. image:: images/crear_campanas/1.PNG
        :align: center

1. Nueva campaña. 

.. image:: images/crear_campanas/2.PNG
        :align: center

2. Nombre de campaña.

3. Se escoje la instancia.

4. Se escoje la calificación.

5. Se escojen a los agentes que se quiera tener en dicha campaña. 

6. Mensaje de inicio de atención (automático).

7. Mensaje de finalización de atención (automático).

8. Guardar.

.. image:: images/crear_campanas/3.PNG
        :align: center

9. Activar campaña.


