********************
Manual de supervisor
********************

Dividiremos el manual de supervisión en diferentes partes con el fin de hacer más específica la 
utilidad de las direntes secciones. 


Dasboard
********

El dashboard del supervisor es el panel de monitoreo diario de los mensajes entrantes, abiertos 
y cerrados que se han gestionado en el día en todas las campañas. Así como el tiempo promedio de 
primera respuesta y de conversación.

Este es la primera vista al entrar a la app como supervisor, de tal forma que tengamos presente 
lo que está pasando con las diferentes atenciones en el día.

.. image:: images/dashboard.PNG
        :align: center

*Figura 1: Dashboard de supervisión*

Usuarios
********

Los usuarios son todos aquellos que tienen acceso a la App de `Mawi <https://www.mawi.chat/>`_  por 
medio del cliente o empresa que adquiere los servicios de la misma. Los usuarios son de dos tipos, 
supervisores y agentes. Los supervisores se encargan de monitorear a los segundos y de hacer otro 
tipo de gestiones en los chats o atenciones. Y los agentes son los que se encargan de atender los 
chats de los clientes que escriben a la empresa, de gestionar sus peticiones y de almacenar la 
información adquirida en las conversaciones con los clientes.

.. image:: images/usuarios.PNG
        :align: center

*Figura 2: Menú de usuarios*

En el menú de usuarios se listan los usuarios que se encuentran creados y sus estados, activos o 
inactivos. También se pueden editar y crear nuevo usuarios.

.. toctree::
  :maxdepth: 0

  usuarios.rst

Gestión
*******

Las gestiones son la forma que tiene el supervisor de la empresa o entidad que contrata de los 
servicios de Mawi para organizar sus campañas y repuestas rápidas de los agentes. En las 
campañas, el usuarios (supervisor), puede organizar las campañas que necesite, las 
calificaciones de las mismas y los formularios que los agentes deben registrar para el 
cierre de las atenciones. 

.. toctree::
  :maxdepth: 2

  gestion.rst

Reportes
********

Los reportes o reportes de gestión son todos aquellas formas de ver la información y/o actividades 
hechas por lo agentes en el transcurso del uso del app. Eso reportes van desde el historial de 
atenciones o chats, haste el reporte de productividad por agente. Los cuales son muy importantes 
para los supervisores de las empresas asociadas de Mawi a la hora de analizar el rendimiento o 
movimientos de sus clientes   por Whapsapp.

.. image:: images/reportes.png
        :align: center

*Figura 3: Menú de reportes*

.. toctree::
  :maxdepth: 2

  reportes.rst

Supervisión
***********

La supervisión es la forma que tienen los supervisores de monitorear que hacen los agentes, en esta 
sección los supervisores también pueden asignar y reasignar atenciones o chats a un agente.

.. toctree::
  :maxdepth: 2

  supervision.rst

Contactos
*********

Los contactos (agenda de contactos), es la sección dispuesta para que los agentes puedan guardar la 
información de los clientes. 

.. toctree::
  :maxdepth: 1

  contactos.rst

Horarios
********

Horarios (u horarios fuera de servicio) es ena sección en la cual los supervisores pueden configurar 
ciertos horarion en donde no habrá agentes en línea y por lo tanto las atenciones no van a ser 
contestadas, pero se le configurará un mensaje personalizado en el cual pueden informar que no habrá 
agentes disponibles.

.. toctree::
  :maxdepth: 1

  horarios.rst